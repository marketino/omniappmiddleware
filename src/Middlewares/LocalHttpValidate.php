<?php

namespace Superius\OmniAppMiddleware\Middlewares;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

use function Safe\parse_url;

class LocalHttpValidate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Auth\AuthenticationException|\Safe\Exceptions\UrlException
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (app()->environment(['local', 'testing'])) {
            return $next($request);
        }

        $urlData = parse_url($request->schemeAndHttpHost());

        if (
            data_get($urlData, 'scheme') === 'http' &&
            Str::endsWith(data_get($urlData, 'host'), config('omniapprouter.aws_local.url_domain')) &&
            data_get($urlData, 'port') === (int)config('omniapprouter.aws_local.port')
        ) {
            return $next($request);
        }

        throw new AuthenticationException('Unauthorized Access');
    }
}
