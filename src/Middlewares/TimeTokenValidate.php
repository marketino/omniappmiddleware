<?php

namespace Superius\OmniAppMiddleware\Middlewares;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Superius\OmniApp\Enums\UserGroupEnum;
use Superius\OmniAppMiddleware\Services\AuthTokenService;

class TimeTokenValidate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        if (!$token) {
            abort(403, 'Missing A2A token');
        }

        $a2aToken = (new AuthTokenService())->fromToken($token);

        if (!$a2aToken->isValid()) {
            abort(403, 'Invalid A2A token');
        }

        if ($a2aToken->hasContextPayload()) {
            $user = new User();
            $user->id = '00000000-0000-0000-0000-000000000001';
            $user->market = $a2aToken->mid;
            $user->tenant_id = $a2aToken->tid;
            $user->group = UserGroupEnum::ADMIN->value;
            $user->is_demo = $a2aToken->is_demo;
            Auth::setUser($user);
        }
        return $next($request);
    }
}
