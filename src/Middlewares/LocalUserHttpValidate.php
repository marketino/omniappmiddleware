<?php

namespace Superius\OmniAppMiddleware\Middlewares;

use App\Models\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Superius\OmniApp\Enums\UserGroupEnum;

class LocalUserHttpValidate extends LocalHttpValidate
{
    /**
     * Handle an incoming request and create a user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Auth\AuthenticationException|\Safe\Exceptions\UrlException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $marketId = $request->header('Market');
        $tenantId = $request->header('Tenant');

        if (!$marketId || !$tenantId) {
            throw new AuthenticationException('Unauthorized Access');
        }

        $user = new User();
        $user->id = '00000000-0000-0000-0000-000000000001';
        $user->market = $marketId;
        $user->tenant_id = $tenantId;
        $user->group = UserGroupEnum::ADMIN->value;
        Auth::setUser($user);

        return parent::handle($request, $next);
    }
}
