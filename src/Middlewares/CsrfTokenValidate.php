<?php

namespace Superius\OmniAppMiddleware\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;

class CsrfTokenValidate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return Closure
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle(Request $request, Closure $next): Closure
    {
        //do not check on unit tests
        if ($this->runningUnitTests()) {
            return $next($request);
        }

        //do not check on reading routes
        if ($this->isReading($request)) {
            return $next($request);
        }

        //do not check on public routes
        if (auth()->guest()) {
            return $next($request);
        }

        //do not check if there is not auth cookie token
        if (!$request->hasCookie("token")) {
            return $next($request);
        }

        //if we have token cookie we MUST have also both: X-XSRF-TOKEN header and XSRF-TOKEN cookie
        if (!$request->hasCookie("XSRF-TOKEN") || !$request->hasHeader("X-XSRF-TOKEN")) {
            throw new TokenMismatchException('Invalid or missing CSRF token.');
        }

        //and both must match
        if ($request->cookie("XSRF-TOKEN") !== $request->header("X-XSRF-TOKEN")) {
            throw new TokenMismatchException('Invalid or missing CSRF token..');
        }

        //this value must be md5 of current cookie auth token
        if ($request->header("X-XSRF-TOKEN") !== md5($request->cookie("token"))) {
            throw new TokenMismatchException('Invalid or missing CSRF token...');
        }

        return $next($request);
    }

    /**
     * Determine if the application is running unit tests.
     */
    protected function runningUnitTests(): bool
    {
        return app()->runningInConsole() && app()->runningUnitTests();
    }

    /**
     * Determine if the HTTP request uses a ‘read’ verb.
     */
    protected function isReading(Request $request): bool
    {
        return in_array($request->method(), ['HEAD', /*'GET',*/ 'OPTIONS']);
    }
}
