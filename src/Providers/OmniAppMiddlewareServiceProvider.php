<?php

namespace Superius\OmniAppMiddleware\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Superius\OmniAppMiddleware\Middlewares\LocalUserHttpValidate;
use Superius\OmniAppMiddleware\Middlewares\TimeTokenValidate;
use Superius\OmniAppMiddleware\Middlewares\CsrfTokenValidate;
use Superius\OmniAppMiddleware\Middlewares\LocalHttpValidate;

class OmniAppMiddlewareServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //getting user for session without db, only from jwt payload
        \Auth::provider('jwt_token_user_provider', function () {
            return resolve(JwtTokenUserProvider::class);
        });

        $router = $this->app['router'];
        $router->aliasMiddleware('a2a_api_token', TimeTokenValidate::class);
        $router->aliasMiddleware('auth.local', LocalHttpValidate::class);
        $router->aliasMiddleware('auth.local.user', LocalUserHttpValidate::class);
        $router->pushMiddlewareToGroup('api', CsrfTokenValidate::class);
    }

    public function boot(): void
    {
        if ($this->app->environment('local')) {
            $source = base_path('vendor/superius/omniapp-middleware/config/omniappmiddleware.php');
            $destination = base_path('config/omniappmiddleware.php');

            File::copy($source, $destination);
        }
    }
}
