<?php

namespace Superius\OmniAppMiddleware\Providers;

use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Superius\OmniApp\Enums\MarketEnum;
use Superius\OmniApp\Enums\UserGroupEnum;
use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Validation\Rules\Enum;
use Validator;

class JwtTokenUserProvider extends EloquentUserProvider
{
    /**
     * CacheUserProvider constructor.
     *
     * @param HasherContract $hasher
     */
    public function __construct(HasherContract $hasher)
    {
        parent::__construct($hasher, User::class);
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier): User|\Illuminate\Contracts\Auth\Authenticatable|null
    {
        $userData = [
            'id' => $identifier,
            'group' => auth()->payload()->get('group'),
            'tenant_id' => auth()->payload()->get('tid'),
            'market' => auth()->payload()->get('market'),
            'is_demo' => auth()->payload()->get('is_demo')
        ];

        $validator = Validator::make($userData, [
            'id' => 'required|uuid',
            'tenant_id' => 'required|uuid',
            'group' => [
                'required',
                new Enum(UserGroupEnum::class)
            ],
            'market' => [
                'required',
                new Enum(MarketEnum::class)
            ],
            'is_demo' => 'required|boolean'
        ]);

        if ($validator->passes()) {
            /** @var User */
            return $this->createModel()->forceFill($validator->validated());
        }

        return null;
    }
}
